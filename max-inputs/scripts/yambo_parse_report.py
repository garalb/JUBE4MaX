#!/usr/bin/env python

import sys

REPORT = sys.argv[1]
OUTPUT = sys.argv[2]


def time_in_seconds(val):
    if "-" in val:
        fields = val.split("-")
        sec=0.0
        for i in fields:
            if "s" in i: sec += float(i.strip("s"))
            if "m" in i: sec += 60 * float(i.strip("m"))
            if "h" in i: sec += 3600 * float(i.strip("h"))
        return sec
    else:
        return float(val.strip("s"))


def parse_report(file_name):
    buffer = []
    resources_dict = {
        "MPI Cores": "mpi", "Threads per core": "thrs", "Threads total": "thr_tot", 
        "Nodes Computing": "nodes", "Nodes IO": "nodes_io", "MPI tasks / GPU": "mpi/gpu",
    }
    resources = {}
    timing = {}
    log = False
    with open(file_name, 'r') as report:
        for line in report:
            if line == "\n": log = False
            if log: 
                buffer.append(line.strip())
            else:
                for key, val in resources_dict.items():
                    if key in line: resources[val] = int(line.strip().split(":")[1])
                if "Time-Profile" in line:
                    timing["Time-Profile"] = time_in_seconds(line.strip().split(":")[1])

            if "Clock:" in line: log = True

    for line in buffer:
        fields = line.split(":")
        timing[fields[0].strip()] = time_in_seconds(fields[1].strip().split()[0])

    timing['Xo'] = timing['Xo (procedure)']
    timing['X'] = timing['X (procedure)']
    timing['Self energy'] = timing['HF'] + timing['GW(ppa)']
    timing['Other'] = timing['Time-Profile'] - timing['Dipoles'] - timing['Xo'] - timing['X'] - timing['Self energy']
    return resources, timing


if __name__ == "__main__":
    resources, timing = parse_report(REPORT)
    with open(OUTPUT, 'w') as fout:
        for key, value in timing.items():
            fout.write('{0}: {1:.4f}\n'.format(key, value))
