# JUBE4MaX

This repository contains 
- max-inputs: JUBE script to benchmark MaX applications on multiple platforms
- examples : some examples to create JUBE scripts

## General information on JUBE

[JUBE website](https://apps.fz-juelich.de/jsc/jube/jube2/docu/index.html#)

Requirements: Python > 3.2

Download from https://www.fz-juelich.de/en/ias/jsc/services/user-support/jsc-softwa
re-tools/jube/download

Install with: `python setup.py install --user`

Commands: `jube {run, continue, analyse, result} <inputfile>`

## How to use max-inputs

To run a benchmark on a given machine from `max-inputs/applications/<app-name>`

`jube run <app-name>.xml --tag <case> <system>`

where 

- `<case>` is the name of the test case among the ones available in `max-inputs/workloads/<app-name>` 
- `<system>` is the name of the machine among the ones available in `max-inputs/platforms/<app-name>`

To print the result table

`jube analyse benchmark-folder`

`jube result benchmark-folder`

